#!/bin/bash
echo "Checking Depedencies for InGIST"

echo "Installing Postgres SQL version 10 Database and PostGIS extension version 2.4"

if [[ `lsb_release -rs` == "16.04" ]]
then
sudo add-apt-repository 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main'
fi

if [[ `lsb_release -rs` == "14.04" ]]
then
sudo add-apt-repository 'deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main'
fi
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-10
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt update
sudo apt install postgis postgresql-10-postgis-2.4 

echo "Creating a role InGist with password InGist in postgres 10"
sudo -u postgres -H -- psql --cluster 10/main -d postgres -c "CREATE ROLE ingist WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'ingist';"
