#                                                       InGIST- Indoor GIS Toolkit

InGIST- Indoor GIS Toolkit is a novel toolkit for indoor spaces that supports configuring indoor spaces and querying over them. It was developed in the Amrita Multidimensional Data Analytics Lab, Amrita Vishwa Vidyapeetham, Coimbatore as part of a project funded by DST, India. It was funded in part by DST(India) grant DyNo.
100/IFD/2764/2012-2013. InGIST implements a hybrid indoor model that effectively combines spatial, geometric and topological models, over which a query language developed as part of this project, operates to support different types of indoor queries. InGIST also supports the IndoorGML standards and allows for exporting to IndoorGML from various formats and querying over IndoorGML.

# This system caters to the following needs:
1. Automatic installation for any building, with focus on efficiency and scalability
2. User friendly, highly-interactive interfaces
3. Functionalities Supported:   
    a. Indoor Information Configuration.  
    b. Indoor Query Interface.  
    c. IndoorGML Graph Representation.  
    d. Sensor Configuration. 
    

# Requirements
<b>OS: Ubuntu</b> (Tested on 16.04 LTS)  
<b>Database: PostgreSQL</b> (This will be automatically installed while running the dependency.sh file)  

# Instructions to run the project

1. Download the project 
2. Give write permission to dependency.sh file.  
	<b><i>Right click the file → Properties → Permissions → check ‘Allow executing file as program’.</i></b>
3. Open a terminal and move into InGIST_V1 directory.
4. Run the dependency.sh file. (It installs postgres10 with postgis-2.4 and creates a database user
    <b><i>ingist</i></b> with password <b><i>ingist</i></b>)  
	<i><b>./dependency.sh</b></i>
5. Double click the executable jar InGIST_V1.jar  
		or  
    run the following command in terminal from directory where executable jar exists.  
     <i><b>java -jar InGIST_V1.jar</b></i>

# For Usage instructions please read the documentation
<i><b>Hope you found the Indoor GIS Toolkit useful!!!! Please send your comments to indooris.amrita@gmail.com. Any queries can be sent to the same email.</b></i>